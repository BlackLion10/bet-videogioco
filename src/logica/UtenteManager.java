package logica;

import java.util.ArrayList;
import java.util.List;

import bean.Partita;
import bean.Utente;
import bean.UtenteVO;
import bean.Validatore;
import dao.GiocoHDao;
import dao.UtenteHDao;

public abstract class UtenteManager {
	
	private static Validatore validatore;
	
	public static Validatore verificaPresenzaUtente(String nick) {
			
		try {	
			ArrayList<Utente> utenti=new ArrayList<Utente>();
			UtenteHDao utenteHDao=new UtenteHDao();
			Utente utente=new Utente();
			for(int i=1;i<7;i++) {
				utente= utenteHDao.getUtenteClass(i);
				utenti.add(utente);
			}
			ArrayList<String> nicknames=new ArrayList();
			for(int j=0;j<utenti.size();j++) {
				nicknames.add(utenti.get(j).getNickname());
			}
			for(int z=0;z<nicknames.size();z++) {
				if(nicknames.get(z).equalsIgnoreCase(nick)) {
					validatore=new Validatore();
					validatore.setCodice(0);
					validatore.setUri("form");
					
				}
				else {
					validatore=new Validatore();
					validatore.setMessaggio("Nickname gi� utilizzato! Scegline un altro!");
					validatore.setCodice(1);
					validatore.setUri("form");
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return validatore;
	}
	
	
	public static Validatore login(String nickname,String password) throws Exception {
		
		UtenteHDao utenteHDao=new UtenteHDao();
		Utente utente= utenteHDao.getUtenteLoginSql(nickname,password);
		if(utente!=null) {
			validatore=new Validatore();
			validatore.setCodice(0);
			validatore.setUri("homepage");
		}
		else {
			validatore=new Validatore();
			validatore.setMessaggio("Inserimento di nickname o password sbagliati");
			validatore.setCodice(1);
			validatore.setUri("homepage");
		}
		return validatore;
		
	}
	
	public static Validatore Classifica() throws Exception {
		GiocoHDao giocoHDao=new GiocoHDao();
		List<Partita> risultati=new ArrayList();
		risultati=giocoHDao.getClassificaCriteria(1);
		System.out.println(risultati.get(0).getPunteggio());
		return validatore;
	}
	
}
