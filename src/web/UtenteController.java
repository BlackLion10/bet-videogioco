package web;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.hibernate.query.Query;
import bean.Validatore;
import gioco.LoginValidator;
import logica.UtenteManager;


@Controller
public class UtenteController {
		
	private Validatore validatore;

	
	@GetMapping("/login")
	public String loginValidator(@RequestParam("nickname") String nickname,@RequestParam("pwd1") String password) throws Exception {
		System.out.println("loggo2");
		validatore=UtenteManager.login(nickname,password);
		return validatore.getUri();
	}
	
	@GetMapping("/login2")
	public String loggo() {
		System.out.println("loggo");
		return "homepage";
	}
		
}
