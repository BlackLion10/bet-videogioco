package bean;

public class UtenteVO extends Utente {

	private int IDUtente;
	
	private  String Nickname;
	
	private String Paese;
	
	public int getIDUtente() {
		return IDUtente;
	}

	public void setIDUtente(int iDUtente) {
		IDUtente = iDUtente;
	}

	public String getNickname() {
		return Nickname;
	}

	public void setNickname(String nickname) {
		Nickname = nickname;
	}

	public String getPaese() {
		return Paese;
	}

	public void setPaese(String paese) {
		Paese = paese;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	private String Password;
}
